#!/bin/bash

KAMAILIO_PATH="../kamailio-5.1.6"

MY_PATH="`dirname \"$0\"`"              # relative
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"  # absolutized and normalized
if [ -z "$MY_PATH" ] ; then
  echo "error; for some reason, the path is not accessible"
  echo "to the script (e.g. permissions re-evaled after suid)"
  exit 1  # fail
fi


if [ ! -d "$KAMAILIO_PATH" ]; then
  echo "Check why path $KAMAILIO_PATH doesn't exist."
  exit 1
fi

#if [ ! -d "$KAMAILIO_PATH/src/modules/presence_queuestate" ]; then
#  echo "Creating source link"
  cp -r  $MY_PATH/src/modules/presence_queuestate $KAMAILIO_PATH/src/modules/presence_queuestate
  cp -r  $MY_PATH/src/modules/pua_rpc_queuestate $KAMAILIO_PATH/src/modules/pua_rpc_queuestate
#fi

cd $KAMAILIO_PATH
if [ ! -f "$KAMAILIO_PATH/src/modules.lst" ]; then
  make cfg
fi
make all

cd $MY_PATH

MOD_PATH=modules
mkdir $MOD_PATH
cp $KAMAILIO_PATH/src/modules/presence_queuestate/*.so $MOD_PATH/
cp $KAMAILIO_PATH/src/modules/pua_rpc_queuestate/*.so $MOD_PATH/

echo ""
echo esrp modules created on $MOD_PATH
ls -la $MOD_PATH/*.so



