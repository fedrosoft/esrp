

local http = require("httpclient")
local m={}

function m.new(t)
  local timeout= t or 5 -- default 5 secs
  local hc= http.new()
--  local inspect = require('inspect')
--  log:info("timeout: %s", inspect(hc:get_defaults()))
  hc:set_default('timeout', timeout)
  return hc
end


return m
