
local logger = {}

--- logger instance with conf Parameters
-- useful to permit reload in main lua script
-- @return logging.rolling_file instance
function logger.new(conf)
  local logging=require "logging"
  logging.rolling_file=require "scripts.ecrfRollingFile"

  local _l=logging.rolling_file(
    conf.file,
    conf.size *1024*1024,
    conf.maxBackupIndex)

  local levels={logging.ERROR, logging.WARN, logging.INFO, logging.DEBUG};
  local l=conf.level +1; -- array index start with 1
  if l<1 then l=1; end
  if l>4 then l=4; end
  _l:setLevel(levels[l])
  return _l
end

return logger;
