

local xmlParser={}
--- extract the content of <location-info> pidf-lo
-- @param1 string, xml pidf-lo
-- @return string or nil, content of location-info (Point, Circle...)
function xmlParser.getLocationInfo(xml)
  local _,_,locationInfo = xml:find("location%-info>(.+)</[%d%a:]*location%-info>")
  if locationInfo then
    locationInfo = locationInfo
                  :gsub("(</[%d%a:]*Point>).*" ,"%1") --filtro eventuali dati aggiuntivi del lis
                  :gsub("(</[%d%a:]*Circle>).*","%1")
  end
  return locationInfo
end
--- extract the <uri> from ecrf (LoST) response
-- @param1 string, xml LoST request
-- @return array or nil, list of  uri to contact (queue uri)
function xmlParser.getUri(xml)
  local array = nil;
  for capture in string.gmatch(xml, "uri>([^</]+)</[%d%a:]*uri>") do
    if array == nil then array={}; end
    table.insert(array, capture) -- ??? todo multiple capture in array
  end
  return array

  -- local _,_,uri = xml:find("uri>([^</]+)</[%d%a:]*uri>"); -- single first capture
  -- return uri
end
--- extract the  <serviceNumber> from ecrf (LoST) response
-- @param1 string, xml LoST request
-- @return string or nil, urn:service associated number (112)
function xmlParser.getServiceNumber(xml)

  local _,_,serviceNumber = xml:find("serviceNumber>([^\n]+)</[%d%a:]*serviceNumber>");
  return serviceNumber
end
return xmlParser;
