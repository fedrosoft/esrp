-- funzioni utili
local function _readAll(file)
    local f = assert(io.open(file, "rb"))
    local content = f:read("*all")
    f:close()
    return content
end

local function _contains(table, val)
   for i=1,#table do
      if table[i] == val then
         return true
      end
   end
   return false
end
-- ------ -- -- -- --

local curpath=debug.getinfo(1,"S").source
	:match[[^@?(.*[\/])[^\/]-$]]
if curpath == nil then curpath=''; end
package.path =  curpath.."?.lua;"
    .. curpath.."../?.lua;"
    .. package.path

lu=require("luaunit")
parser=require("scripts.xmlParser")

function testGetLocationInfoVoid()
  local xml=""
  local x=parser.getLocationInfo(xml)
  lu.assertEquals(x,nil)
end

function testGetLocationInfoTest1()
  local xml=_readAll('test1.xml')
  local x=parser.getLocationInfo(xml)
--  print (x.."\n");
  local expected=[[<gml:Point srsName="urn:ogc:def:crs:EPSG::4326">
           <gml:pos>45.5 9.2</gml:pos>
         </gml:Point>]]
  lu.assertEquals(x:gsub("%s+", ""),expected:gsub("%s+", ""))
end

function testGetLocationInfoCircle()
  local xml=[[<gp:geopriv>
           <gp:location-info> <gml:Circle id="circle1" srsName="geodetic-2d"><gml:pos>40.730610 -73.935242</gml:pos><gml:radius>100</gml:radius></gml:Circle> </gp:location-info>
           <gp:usage-rules>
           </gp:usage-rules>
           <gp:method>802.11</gp:method>
         </gp:geopriv>
]]
  local x=parser.getLocationInfo(xml)
--  print (x.."\n");
  local expected=[[<gml:Circle id="circle1" srsName="geodetic-2d">
    <gml:pos>40.730610 -73.935242</gml:pos>
    <gml:radius>100</gml:radius>
  </gml:Circle>]]
  lu.assertEquals(x:gsub("%s+", ""),expected:gsub("%s+", ""))
end


local xmlFindServiceResponseMultipleUri=[[<findServiceResponse>
<mapping expires="0001-01-01T00:00:00Z" lastUpdated="0001-01-01T00:00:00Z">
<service>urn:service:sos.animal-control</service>
<serviceBoundary profile="geodetic-2d">
<Polygon>
<exterior>
<LinearRing>
<posList>35.900415887704668 -80.221987718640293 35.900415887704668 -67.673796819960955 45.445719869631574 -67.673796819960955 45.445719869631574 -80.221987718640293 35.900415887704668 -80.221987718640293</posList>
</LinearRing>
</exterior>
</Polygon>
</serviceBoundary>
<uri>sip:newyork.animalcontrol.sip</uri>
<uri>sip:newyork.pd</uri>
<serviceNumber>354</serviceNumber>
</mapping>
<locationUsed />
</findServiceResponse>
]]
function testGetMultipleUri()
  local x=parser.getUri(xmlFindServiceResponseMultipleUri);

  lu.assertEquals(_contains(x, "sip:newyork.animalcontrol.sip"), true)
  lu.assertEquals(_contains(x, "sip:newyork.pd"), true)
end

function testGetServiceNumber()
  local x=parser.getServiceNumber(xmlFindServiceResponseMultipleUri);
  lu.assertEquals(x, "354")
end


os.exit( lu.LuaUnit.run() )
