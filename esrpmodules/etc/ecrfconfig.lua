local ecrfconfig={}

ecrfconfig.esrp={
  INSTANCE={
    name="paperino",
    type="originating",
    configurationUrl="http://10.200.200.80/getconfig/",
    hackRuriTo = true,
    httpTimeout = 15,
    log={
      file = "/tmp/ecrf.log",
      size = 10, -- in MBytes
      maxBackupIndex = 5,
      level = 3, -- 0:error, 1:warn, 2:info, 3:debug
    }
  },
  LIS={
    url ="http://172.16.11.172/LIS/api/v1/Location?agency=NG&customer=NG&service=NG",
    xml=[[<?xml version="1.0" encoding="utf-8"?>
    <locationRequest xmlns="urn:ietf:params:xml:ns:geopriv:held" responseTime="8">
      <locationType exact="true">geodetic</locationType>
      <device xmlns="urn:ietf:params:xml:ns:geopriv:held:id">
        <msisdn>_MSISDN_</msisdn>
      </device>
    </locationRequest>
    ]]
  },
  ECRF={
    url="http://10.200.200.80/ecrf/index.php?server=",
--    url="http://172.16.11.172:81/api/lost/",
    xml=[[<?xml version="1.0" encoding="UTF-8"?>
    <findService xmlns="urn:ietf:params:xml:ns:lost1"
                 xmlns:gml="http://www.opengis.net/gml"
                 xmlns:gs="http://www.opengis.net/pidflo/1.0"
                 xmlns:p2="http://www.opengis.net/"
           recursive="true" serviceBoundary="value">
      <location id="ecrf_1t3b3w" profile="geodetic-2d">
         _LOCATION-INFO_
      </location>
      <service>_SERVICE-URN_</service>
    </findService>
    ]]
  }
}

function ecrfconfig.reload(luareload)
  local luareload=luareload or 'true';
  local httpclient=require "httpclient"
  local hc = httpclient.new();
  local jsonLua = require "JSON"
  local json = jsonLua:encode(ecrfconfig.esrp.INSTANCE)
  local res = hc:post(ecrfconfig.esrp.INSTANCE.configurationUrl, json, {content_type = "application/json"})
  --KSR.info("reload response: "..res.body.."\n");
  -- aggiornare esrp
  ecrfconfig.esrp.INSTANCE=jsonLua:decode(res.body)
  -- richiamare rpc reload
  local ret='';
  if luareload == 'true' then
--    KSR.info("reload response: "..res.body.."\n");
    log:info("getConfig response: "..res.body.."\n");

    KSR.jsonrpcs.exec('{"jsonrpc": "2.0", "method": "app_lua.reload", "id": 1}');
    ret=KSR.pv.get("$jsonrpl(body)");
  end
  return ret;
end

function ecrfconfig.getLisUrl()
  return ecrfconfig.esrp.LIS.url;
end
function ecrfconfig.getLisXml(msisdn)
  return ecrfconfig.esrp.LIS.xml:gsub("_MSISDN_",msisdn);
end
function ecrfconfig.getEcrfUrl()
  local str=ecrfconfig.esrp.ECRF.url;
  local ending="server=";
  if str:sub(-#ending) == ending then-- in caso server interno test.. aggiunge ip
    str = str..KSR.pv.gete("$si");
  end
  return str;
end
function ecrfconfig.getEcrfXml(locationInfo, serviceUrn)
  return ecrfconfig.esrp.ECRF.xml:gsub("_LOCATION%-INFO_", locationInfo):gsub("_SERVICE%-URN_",serviceUrn);
end
function ecrfconfig.getLog()
  return ecrfconfig.esrp.INSTANCE.log
end

function ecrfconfig.getHackRuriTo()
  return ecrfconfig.esrp.INSTANCE.hackRuriTo
end

function ecrfconfig.getHttpTimeout()
  return ecrfconfig.esrp.INSTANCE.httpTimeout
end

return ecrfconfig;
